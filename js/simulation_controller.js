// global constants
const READY = 0, RUNNING = 1, STOPPED = 2, FINISHED = 3; // 4 states of simulation
const MINIMUM_WAIT_TIME = 5; // minimum amount of ms waiting between the steps
const MAXIMUM_WAIT_TIME = 1000;

/**
 * Class for controlling the simulation
 * It communicates with DOM and CanvasController class
 */
class SimulationController {
    /**
     * n -> columns
     * m -> rows
     * maze -> null if we create the simulation from scratch (not loading from localStorage)
     */
    constructor(n, m, maze) {
        this.m = m;
        this.n = n;
        // creates new instation of CanvasController
        this.canvasController = new CanvasController(n, m);
        this.canvasController.init();
        this.initGlobals();
        // create new maze if needed
        if (maze != null) {
            this.maze = maze;
            // find start and destination point of given maze
            this.findStartAndDest();
        } else {
            this.initMaze();
        }
        this.initListeners();
        this.redrawCanvas();
    }

    // initializes global class attributes
    initGlobals() {
        // list of valid directions
        this.DIRECTIONS = [[0, 1], [1, 0], [-1, 0], [0, -1]];
        // uncomment this to use 8-way pathfinder
        // this.DIRECTIONS = [[0, 1], [1, 0], [-1, 0], [0, -1], [1, 1], [1, -1], [-1, 1], [1, -1]];
        this.nDir = this.DIRECTIONS.length;

        // Constants:
        this.NOTHING = 0;
        this.WALL = 1;
        this.START = 2;
        this.DEST = 3;
        this.CLOSED_LIST = 4;
        this.OPEN_LIST = 5;
        this.PATH = 6;

        // Slider for interval:
        this.slider = document.querySelector('#slider_wrap div + input');
        this.sliderMax = this.slider.max;

        // flags for start and destination point setting
        this.setStart = false;
        this.setDestination = false;

        // width and height of boxes (should be the same in this app)
        this.boxWidth = this.canvasController.canvas.width / this.n;
        this.boxHeight = this.canvasController.canvas.height / this.m;
        if (this.boxHeight - this.boxWidth > 0.05) {
            console.log('Warning, boxWidth != boxHeight!');
            console.log(this.boxWidth + ' ' + this.boxHeight);
        }

        // set default startRestartButton and destination positions
        this.start = {x: 0, y: 0};
        this.destination = {x: this.n - 1, y: this.m - 1};

        // set state of simulation to ready
        this.STATE = READY;

        // set interval of canvas refreshing in ms
        this.intervalCoefficient = (MAXIMUM_WAIT_TIME - MINIMUM_WAIT_TIME) / this.sliderMax;

        // set sound to play when goal is reached
        this.goal_sound = new Audio('sounds/goal.wav');
        this.goal_sound.volume = 0.2;
    }

    // initializes new maze (2D array of integers)
    initMaze() {
        const m = this.m, n = this.n;
        this.maze = new Array(m);
        for (let i = 0; i < m; i++) {
            this.maze[i] = new Array(n);
            for (let j = 0; j < n; j++) {
                this.maze[i][j] = this.NOTHING;
            }
        }
        // set start and dest point
        this.maze[this.start.y][this.start.x] = this.START;
        this.maze[this.destination.y][this.destination.x] = this.DEST;
    }

    // finds where the points are in the maze
    findStartAndDest() {
        for (let i = 0; i < this.m; i++) {
            for (let j = 0; j < this.n; j++) {
                if (this.maze[i][j] === this.START) {
                    this.start = {x: j, y: i};
                } else if (this.maze[i][j] === this.DEST) {
                    this.destination = {x: j, y: i};
                }
            }
        }
    }

    // initializes eventListeners on canvas
    initListeners() {
        // flag for canvas drawing
        let flagMousedown = false;
        const canvas = this.canvasController.canvas;
        // computes offset
        const offsetX = Math.round(parseFloat(canvas.style.left) * window.innerWidth / 100 + BORDER_WIDTH);
        const offsetY = Math.round(parseFloat(canvas.style.top) * window.innerHeight / 100 + BORDER_WIDTH);
        // event listener for mousemove
        canvas.addEventListener('mousemove', function (event) {
            if (flagMousedown && (this.STATE === READY || this.STATE === STOPPED)) {
                const x = event.clientX - offsetX, y = event.clientY - offsetY;
                // when mouse down flag is true and the state is READY or STOPPED call evaluate (with non-negative values)
                this.evaluate(x > 0 ? x : 0, y > 0 ? y : 0);
            }
        }.bind(this));
        // event listener for mousedown
        canvas.addEventListener('mousedown', function (event) {
            if (this.setStart) { // if someone clicked on set start
                const x = event.clientX - offsetX, y = event.clientY - offsetY;
                // compute x and y coordination and call setStartAt
                this.setStartAt(x > 0 ? x : 0, y > 0 ? y : 0);
                this.setStart = false; // turn off the flag
                // set font weight back to normal
                this.setFontWeight(document.getElementById('button_set_start'), 400);
            } else if (this.setDestination) {
                const x = event.clientX - offsetX, y = event.clientY - offsetY;
                this.setDestinationAt(x > 0 ? x : 0, y > 0 ? y : 0);
                this.setDestination = false;
                this.setFontWeight(document.getElementById('button_set_dest'), 400);
            } else if (!flagMousedown && (this.STATE === READY || this.STATE === STOPPED)) {
                // call only if flagMousedown is false => calling only once
                const x = event.clientX - offsetX, y = event.clientY - offsetY;
                this.evaluate(x > 0 ? x : 0, y > 0 ? y : 0);
                flagMousedown = true;
            }
        }.bind(this));
        document.body.addEventListener('mouseup', function () {
            // set flag back to false
            flagMousedown = false;
        }.bind(this));
    }

    // called when clicked on start/restart button
    startRestartButton() {
        const btn = document.getElementById('button_start_reset');
        // if state is READY and flags setDestination and setStart are false
        if (this.STATE === READY && !this.setDestination && !this.setStart) {
            this.STATE = RUNNING; // change state
            this.openList = new OpenList(this.m, this.n); // create new OpenList
            this.closedList = new ClosedList(this.m, this.n); // create new ClosedList
            this.openList.add(this.start.x, this.start.y, 0, 0, this.start.x, this.start.y); // add start to OpenList
            btn.innerText = 'Reset'; // change button text
            this.nextStep(); // call next step of simulation
        } else if (this.STATE !== READY) { // if state isn't ready just restart maze
            this.STATE = READY;
            for (let i = 0; i < this.m; i++) {
                for (let j = 0; j < this.n; j++) {
                    if (this.maze[i][j] !== this.WALL && this.maze[i][j] !== this.START && this.maze[i][j] !== this.DEST) {
                        this.maze[i][j] = this.NOTHING;
                    }
                }
            }
            btn.innerText = 'Start';
            // reset the text on pause button
            document.getElementById('button_pause_resume').innerText = 'Pause';
            this.redrawCanvas(); // redraw canvas when finished
        }
        if (SOUND_ON) {
            CLICK_SOUND.play();
        }
    }

    // stops the simulation and clear all the walls in maze
    clearButton() {
        this.STATE = READY;
        for (let i = 0; i < this.m; i++) {
            for (let j = 0; j < this.n; j++) {
                if (this.maze[i][j] !== this.START && this.maze[i][j] !== this.DEST) {
                    this.maze[i][j] = this.NOTHING;
                }
            }
        }

        // set flags to false and set font weight on buttons back to normal
        this.setDestination = false;
        this.setStart = false;
        this.setFontWeight(document.getElementById('button_set_start'), 400);
        this.setFontWeight(document.getElementById('button_set_dest'), 400);

        // reset the pause button
        document.getElementById('button_pause_resume').innerText = 'Pause';
        document.getElementById('button_start_reset').innerText = 'Start';
        this.redrawCanvas();
        if (SOUND_ON) {
            CLICK_SOUND.play();
        }
    }

    // pauses the simulation
    pauseResumeButton() {
        const btn = document.getElementById('button_pause_resume');
        if (this.STATE === RUNNING) { // when running -> stopped
            this.STATE = STOPPED;
            btn.innerText = 'Resume';
        } else if (this.STATE === STOPPED) { // when stopped -> running
            this.STATE = RUNNING;
            btn.innerText = 'Pause';
            this.nextStep(); // call nextStep to continue in simulation
        }
        if (SOUND_ON) {
            CLICK_SOUND.play();
        }
        this.redrawCanvas();
    }

    // sets font-weight of elem to value
    setFontWeight(elem, value) {
        elem.style.setProperty('font-weight', String(value));
    }

    // sets flag for setting start to true/false depending on current value
    setStartButton() {
        if (this.STATE === READY && this.setDestination === false) {
            if (this.setStart) {
                this.setStart = false;
                this.setFontWeight(document.getElementById('button_set_start'), 400);
            } else {
                this.setStart = true;
                this.setFontWeight(document.getElementById('button_set_start'), 900); // bold text
            }

        }
        if (SOUND_ON) {
            CLICK_SOUND.play();
        }
    }

    setDestinationButton() {
        if (this.STATE === READY && this.setStart === false) {
            if (this.setDestination) {
                this.setDestination = false;
                this.setFontWeight(document.getElementById('button_set_dest'), 400);
            } else {
                this.setDestination = true;
                this.setFontWeight(document.getElementById('button_set_dest'), 900);
            }
        }
        if (SOUND_ON) {
            CLICK_SOUND.play();
        }
    }

    // sets flag for deleting to true/false depending on current value
    deleteSwitchButton() {
        if (this.STATE === READY) {
            if (this.DELETE) {
                this.DELETE = false;
                this.setFontWeight(document.getElementById('button_del_flag'), 400);
            } else {
                this.DELETE = true;
                this.setFontWeight(document.getElementById('button_del_flag'), 900);
            }
        }
        if (SOUND_ON) {
            CLICK_SOUND.play();
        }
    }

    // saves walls and start and dest points in maze to localStorage
    saveButton() {
        // parse from JSON format
        let mazes = JSON.parse(localStorage.getItem('saves'));
        if (mazes === null) {
            mazes = [];
        }
        if (mazes.length < 9) {
            // push to mazes
            mazes.push({maze: this.getEditedMaze(), date: new Date()});
            // get json text format
            const json = JSON.stringify(mazes);
            // save to localStorage
            localStorage.setItem('saves', json);
        } else {
            alert('Local Storage is full, please delete older saves.');
        }

        if (SOUND_ON) {
            CLICK_SOUND.play();
        }
    }

    // returns new maze only with walls and start and dest points
    getEditedMaze() {
        let ret = new Array(this.maze.length);
        for (let i = 0; i < this.maze.length; i++) {
            ret[i] = new Array(this.maze[0].length);
            for (let j = 0; j < this.maze[0].length; j++) {
                const tmp = this.maze[i][j];
                if (tmp === this.NOTHING || tmp === this.WALL || tmp === this.START || tmp === this.DEST) {
                    ret[i][j] = this.maze[i][j];
                } else {
                    ret[i][j] = this.NOTHING;
                }
            }
        }
        return ret;
    }

    // draws or deletes wall
    // x and y are pixel positions
    evaluate(x, y) {
        x = Math.floor(x / this.boxWidth);
        y = Math.floor(y / this.boxHeight);
        const _x = x === this.n ? this.n - 1 : x; // column
        const _y = y === this.m ? this.m - 1 : y; // row
        if (this.maze[_y][_x] === this.WALL && this.DELETE) {
            this.maze[_y][_x] = this.NOTHING;
            this.redrawCanvas();
        } else if (this.maze[_y][_x] === this.NOTHING && !this.DELETE) {
            this.maze[_y][_x] = this.WALL;
            this.redrawCanvas();
        }
    }

    // redraws canvas using canvas controller
    redrawCanvas() {
        this.canvasController.bg(); // draws background
        // draws every square in maze
        for (let i = 0; i < this.m; i++) {
            for (let j = 0; j < this.n; j++) {
                if (this.maze[i][j] === this.WALL) {
                    this.canvasController.drawSquareAt(j, i, SQUARE_COLOR_WALL, -1);
                } else if (this.maze[i][j] === this.START) {
                    this.canvasController.drawSquareAt(j, i, SQUARE_COLOR_START, SQUARE_OFFSET);
                } else if (this.maze[i][j] === this.DEST) {
                    this.canvasController.drawSquareAt(j, i, SQUARE_COLOR_DESTINATION, SQUARE_OFFSET);
                } else if (this.maze[i][j] === this.CLOSED_LIST) {
                    this.canvasController.drawSquareAt(j, i, SQUARE_COLOR_CLOSED_LIST, SQUARE_OFFSET);
                } else if (this.maze[i][j] === this.OPEN_LIST) {
                    this.canvasController.drawSquareAt(j, i, SQUARE_COLOR_OPEN_LIST, SQUARE_OFFSET);
                } else if (this.maze[i][j] === this.PATH) {
                    this.canvasController.drawSquareAt(j, i, SQUARE_COLOR_PATH, SQUARE_OFFSET);
                }
            }
        }
        this.canvasController.grid(); // draws grid
    }

    // calls next step o simulation
    nextStep() {
        if (this.STATE !== RUNNING) { // if state isn't running don't do anything
            return;
        }

        if (this.openList.isEmpty()) { // if openList is empty -> there is no path -> alert and set state to finished
            this.STATE = FINISHED;
            alert('There is no path from Start to Destination point!');
            return;
        }

        const t = Math.floor(performance.now()); // save time before performing the algorithm
        const node = this.openList.getNext(); // get next node from openList (priority queue)
        this.closedList.add(node.x, node.y); // add node coordinates to closed list
        if (this.maze[node.y][node.x] !== this.START && this.maze[node.y][node.x] !== this.DEST) {
            // if the point in maze is not start or dest point mark it as closed list
            this.maze[node.y][node.x] = this.CLOSED_LIST;
        }

        // expand in every direction
        for (let i = 0; i < this.nDir; i++) {
            // get row and column
            const x = node.x + this.DIRECTIONS[i][0];
            const y = node.y + this.DIRECTIONS[i][1];
            // if x and y is in maze range
            if (x >= 0 && x < this.n && y >= 0 && y < this.m) {
                // x and y not in closed list && field is not a wall
                if (!this.closedList.contains(x, y) && this.maze[y][x] !== this.WALL) {
                    const cost = node.cost + 1; // get cost of parent node and add 1
                    const heur = cost + this.getHeuristicCost(x, y); // add heuristic cost
                    if (!this.openList.contains(x, y)) { // not in open list
                        this.openList.add(x, y, cost, heur, node.x, node.y); // add to open list (node.x and node.y is parent node)
                        if (this.maze[y][x] !== this.START && this.maze[y][x] !== this.DEST) {
                            this.maze[y][x] = this.OPEN_LIST; // mark as OPEN_LIST
                        }
                    } else { // point already is in openList -> update value
                        if (heur < this.openList.getHeuristicAt(x, y)) {
                            this.openList.update(x, y, cost, heur, node.x, node.y);
                        }
                    }
                }
            }
        }

        // we found our destination point
        if (node.x === this.destination.x && node.y === this.destination.y) {
            this.STATE = FINISHED;
            this.addPathToCanvas();
            this.redrawCanvas();
            if (SOUND_ON) {
                this.goal_sound.play();
            }
            return;
        }

        this.redrawCanvas(); // at every step, after performing the algorithm, redraw canvas
        const timeDif = Math.floor(performance.now()) - t; // compute how much time the algorithm and drawing took
        // set waitingTime according to the timeDif
        // in this app timeDif is usually very small (0 - 10 ms), so it does not really make sense to do it like that
        const waitingTime = MINIMUM_WAIT_TIME + (this.sliderMax - parseInt(this.slider.value)) * this.intervalCoefficient - timeDif;
        setTimeout(this.nextStep.bind(this), waitingTime > 0 ? waitingTime : 0); // call nextStep with computed waitingTime
    }

    // returns Manhattan distance from x y to west point
    getHeuristicCost(x, y) {
        // uncomment for using Dijkstra algorithm instead of A*
        //return 0; // no heuristic
        return Math.abs(x - this.destination.x) + Math.abs(y - this.destination.y);
    }

    // sets start (! x and y are pixel distances !)
    setStartAt(x, y) {
        this.maze[this.start.y][this.start.x] = this.NOTHING;
        x = Math.floor(x / this.boxWidth);
        y = Math.floor(y / this.boxHeight);
        const _x = x === this.n ? this.n - 1 : x; // column
        const _y = y === this.m ? this.m - 1 : y; // row
        if (this.maze[_y][_x] === this.NOTHING) {
            this.maze[_y][_x] = this.START;
            this.start.x = _x;
            this.start.y = _y;
            this.redrawCanvas();
        }
    }

    // sets destination (same as setStartAt)
    setDestinationAt(x, y) {
        this.maze[this.destination.y][this.destination.x] = this.NOTHING;
        x = Math.floor(x / this.boxWidth);
        y = Math.floor(y / this.boxHeight);
        const _x = x === this.n ? this.n - 1 : x;
        const _y = y === this.m ? this.m - 1 : y;
        if (this.maze[_y][_x] === this.NOTHING) {
            this.maze[_y][_x] = this.DEST;
            this.destination.x = _x;
            this.destination.y = _y;
            this.redrawCanvas();
        }
    }

    // add path from start to dest on canvas
    addPathToCanvas() {
        if (this.STATE !== FINISHED) { // when state is not FINISHED don't do anything
            return;
        }
        let current = this.openList.getNodeAt(this.destination.x, this.destination.y).from; // get parent of destination node
        while (current.x !== this.start.x || current.y !== this.start.y) { // while current node isn't start node
            this.maze[current.y][current.x] = this.PATH; // change the field in canvas to PATH
            current = current.from; // get parent
        }
    }
}