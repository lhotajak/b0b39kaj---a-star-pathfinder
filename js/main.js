// global flag for turning sounds on/off
let SOUND_ON = true;

// load sounds
const START_SOUND = new Audio('sounds/start.wav');
START_SOUND.volume = 0.2;

const CLICK_SOUND = new Audio('sounds/click.wav');
CLICK_SOUND.volume = 0.2;

const bgMusic = document.getElementById('bg_music');
bgMusic.volume = 0.1;

// using jQuery
$(function () {
    // global variable for manipulation with simulations
    let simulatorController = null;

    // calling initialization functions
    initListeners();
    route();

    // initializing listeners on buttons
    function initListeners() {
        // button for generating new canvas
        $('#button_generate').click(() => start(null));

        // listeners for turning sounds on/off
        const volOn = $('#volume_on');
        volOn.click(() => {
            SOUND_ON = false;
            volOn.css('visibility', 'hidden');
            volOff.css('visibility', 'visible');
        });
        // set default value
        volOn.css('visibility', 'visible');

        const volOff = $('#volume_off');
        volOff.click(() => {
            SOUND_ON = true;
            volOn.css('visibility', 'visible');
            volOff.css('visibility', 'hidden');
            CLICK_SOUND.play();
        });
        volOff.css('visibility', 'hidden');

        // button for returning back to menu
        $('#back_to_menu').click(backToMenu);

        // whenever url changes call route
        window.addEventListener('popstate', () => {
            if (SOUND_ON) {
                CLICK_SOUND.play();
            }
            route();
        });

        // simple connection check
        // after clicking on svg element it checks whether the client is connected to the internet using navigator.onLine utility
        const connectionCheck = $('#connection_check');
        const connResp = $('#connection_check_response');
        connectionCheck.click(() => {
            connectionCheck.css('opacity', '1');
            if (navigator.onLine) {
                // draw content according to the result of connection check
                greenHappySvgFace();
                connResp.text('You are connected!');
                setTimeout(() => {
                    yellowNeutralFace();
                    connResp.text('');
                }, 3000);
            } else {
                redSadSvgFace();
                connResp.text('You are NOT connected!');
                setTimeout(() => {
                    yellowNeutralFace();
                    connResp.text('');
                }, 3000);
            }
        });
    }

    /**
     * starting point of the simulation
     * creates canvas and initializes simulation controller
     * if maze == null then creates new maze based on given size
     */
    function start(maze) {
        let w, h;
        if (maze == null) {
            // get proportions from input elements
            w = parseInt(document.getElementById('width').value);
            h = parseInt(document.getElementById('height').value);
            if (!(w >= 1 && w <= 140 && h >= 1 && h <= 80)) {
                return;
            }
        } else {
            // get width and height from given maze
            w = maze[0].length;
            h = maze.length;
        }

        // if sounds are allowed play start sound
        if (SOUND_ON) {
            START_SOUND.play();
        }

        // temporarily remove main_menu element
        $('#main_menu').css('display', 'none');

        // create new simulation controller
        simulatorController = new SimulationController(w, h, maze);

        // show control panel and slider
        $('#control_panel').css('visibility', 'visible');
        $('#slider_wrap').css('visibility', 'visible');

        // initialize slider and attach an eventListener to it
        const sliderValue = $('#slider_wrap div:last-child');
        const slider = $('#slider_wrap div + input');
        const domSlider = slider[0]; // slider[0] is DOM object
        const maxValue = slider.attr('max');
        sliderValue.text(Math.round(domSlider.value * 100 / maxValue).toString() + '%');
        domSlider.addEventListener('input', () => {
            sliderValue.text(Math.round(domSlider.value * 100 / maxValue).toString() + '%');
        });

        // initialize eventListeners for controlling the simulation
        $('#button_start_reset').click(simulatorController.startRestartButton.bind(simulatorController));
        $('#button_pause_resume').click(simulatorController.pauseResumeButton.bind(simulatorController));
        $('#button_clear').click(simulatorController.clearButton.bind(simulatorController));
        $('#button_set_start').click(simulatorController.setStartButton.bind(simulatorController));
        $('#button_set_dest').click(simulatorController.setDestinationButton.bind(simulatorController));
        $('#button_del_flag').click(simulatorController.deleteSwitchButton.bind(simulatorController));
        $('#button_save').click(simulatorController.saveButton.bind(simulatorController));
    }

    // removes canvas and every process running in the background
    function clearSimulation() {
        // calling clearButton() stops the execution on background
        simulatorController.clearButton();

        // remove canvas, hide panel and slider
        $('#maze_canvas').remove();
        $('#control_panel').css('visibility', 'hidden');
        $('#slider_wrap').css('visibility', 'hidden');

        // remove eventListeners
        $('#button_start_reset').off('click');
        $('#button_pause_resume').off('click');
        $('#button_clear').off('click');
        $('#button_set_start').off('click');
        $('#button_set_dest').off('click');
        $('#button_del_flag').off('click');
        $('#button_save').off('click');

        // show main_menu container
        $('#main_menu').css('display', 'block');

        // set pointer of the old simulation to null
        simulatorController = null;
    }

    // stops the simulation, removes canvas and shows the main menu
    function backToMenu() {
        // if simulatorController has not been initialized yet just go back to main menu
        if (simulatorController == null) {
            if (window.location.hash === '#localstorage') {
                // if the previous content was local storage go back to main menu
                window.location.hash = '#mainmenu';
            } else {
                // otherwise just play sound
                if (SOUND_ON) {
                    CLICK_SOUND.play();
                }
            }
            return;
        }

        // removes canvas and every process running in the background
        clearSimulation();

        // choose content (localStorage vs. main menu generator)
        route();
    }

    // displays saved mazes
    function showLocalStorage() {
        // hide the "inside" element in main_menu
        $('#main_menu_inside').css('display', 'none');

        // create container for saved mazes
        $('#main_menu').append('<section id=\'local_storage_container\'></section>');

        // append saves
        const json_obj = localStorage.getItem('saves');
        const mazes = JSON.parse(json_obj);
        if (mazes !== null) {
            mazes.forEach(addMaze);
        }

        // add button to get back to the main menu (inside element)
        addBackButton();
    }

    // appends saved maze to local_storage_container
    function addMaze(data) {
        // data.maze, data.date
        // get formatted date
        const formatted_date = getFormattedDate(new Date(data.date));

        // add formatted date
        const elem = document.createElement('div');
        elem.setAttribute('class', 'saved_maze');
        elem.innerText = formatted_date;

        // add button for loading and deleting
        const btnLoad = document.createElement('button');
        btnLoad.setAttribute('class', 'saved_maze_btn');
        btnLoad.innerText = 'Load';

        const btnDel = document.createElement('button');
        btnDel.setAttribute('class', 'saved_maze_del_btn');
        btnDel.innerText = 'Delete';

        elem.appendChild(btnLoad);
        elem.appendChild(btnDel);
        document.getElementById('local_storage_container').appendChild(elem);

        // attach eventListeners for loading and removing from storage
        btnDel.addEventListener('click', () => {
            removeFromStorage(data, elem);
        });
        btnLoad.addEventListener('click', () => {
            // start new simulation
            start(data.maze);
            // revert changes back to main_menu
            document.getElementById('main_menu').removeChild(document.getElementById('local_storage_container'));
            document.getElementById('main_menu_inside').style.setProperty('display', 'block');
        });
    }

    // removes element from DOM and data from localStorage
    function removeFromStorage(data, elem) {
        document.getElementById('local_storage_container').removeChild(elem);

        // get mazes, get index of wanted element, delete that element and save
        const mazes = JSON.parse(localStorage.getItem('saves'));
        const idx = mazes.indexOf(data);
        mazes.splice(idx, 1);
        const json = JSON.stringify(mazes);
        localStorage.setItem('saves', json);

        if (SOUND_ON) {
            CLICK_SOUND.play();
        }
    }

    function getFormattedDate(date) {
        return date.getDate() + '.' + (date.getMonth() + 1) + '.' + date.getFullYear() + '; ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
    }

    // adds button for returning to main menu
    function addBackButton() {
        $('#local_storage_container').append('<a id="button_back_from_loading" class="button_style" href="#mainmenu">Back</a>');
    }

    // returns to main menu
    function showMainMenu() {
        $('#local_storage_container').remove();
        $('#main_menu_inside').css('display', 'block');
    }

    // routes to the content in page
    function route() {
        // if the simulation is still running stop it
        if (simulatorController !== null) {
            clearSimulation();
        }
        if (window.location.hash === '#localstorage') {
            showLocalStorage();
        } else {
            showMainMenu();
        }
    }

    // draws green smiling face
    function greenHappySvgFace() {
        // save namespace
        const NS = 'http://www.w3.org/2000/svg';
        // get element
        const svg = document.getElementById('connection_check');
        // delete everything
        svg.innerHTML = '';
        // draw content
        drawSvgCircle(svg, NS, 50, 50, 40, '#62ff43');
        drawSvgCircle(svg, NS, 35, 35, 5, 'black');
        drawSvgCircle(svg, NS, 65, 35, 5, 'black');
        createHappyPath(svg, NS);
    }

    function drawSvgCircle(svg, NS, x, y, r, color) {
        const circle = document.createElementNS(NS, 'circle');
        circle.setAttributeNS(null, 'cx', x);
        circle.setAttributeNS(null, 'cy', y);
        circle.setAttributeNS(null, 'r', r);
        circle.setAttributeNS(null, 'fill', color);
        svg.appendChild(circle);
    }

    // draws smiling curve ( ∪ )
    function createHappyPath(svg, NS) {
        const path = document.createElementNS(NS, 'path');
        path.setAttributeNS(null, 'd', 'M 25 60 Q 50 90 75 60');
        path.setAttributeNS(null, 'stroke', 'black');
        path.setAttributeNS(null, 'stroke-width', '8px');
        path.setAttributeNS(null, 'stroke-linejoin', 'round');
        path.setAttributeNS(null, 'stroke-linecap', 'round');
        path.setAttributeNS(null, 'fill', 'transparent');
        svg.appendChild(path);
    }

    function redSadSvgFace() {
        const NS = 'http://www.w3.org/2000/svg';
        const svg = document.getElementById('connection_check');
        svg.innerHTML = '';
        drawSvgCircle(svg, NS, 50, 50, 40, 'red');
        drawSvgCircle(svg, NS, 35, 35, 5, 'black');
        drawSvgCircle(svg, NS, 65, 35, 5, 'black');
        createSadPath(svg, NS);
    }

    // draws sad curve ( ∩ )
    function createSadPath(svg, NS) {
        const path = document.createElementNS(NS, 'path');
        path.setAttributeNS(null, 'd', 'M 25 70 Q 50 40 75 70');
        path.setAttributeNS(null, 'stroke', 'black');
        path.setAttributeNS(null, 'stroke-width', '8px');
        path.setAttributeNS(null, 'stroke-linejoin', 'round');
        path.setAttributeNS(null, 'stroke-linecap', 'round');
        path.setAttributeNS(null, 'fill', 'transparent');
        svg.appendChild(path);
    }

    function yellowNeutralFace() {
        const NS = 'http://www.w3.org/2000/svg';
        const svg = document.getElementById('connection_check');
        svg.innerHTML = '';
        drawSvgCircle(svg, NS, 50, 50, 40, 'yellow');
        drawSvgCircle(svg, NS, 35, 35, 5, 'black');
        drawSvgCircle(svg, NS, 65, 35, 5, 'black');
        createHappyPath(svg, NS);
    }
});
