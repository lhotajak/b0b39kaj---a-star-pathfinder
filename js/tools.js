/**
 * Class with implementation of indexed priority queue
 */
class IndexedPriorityQueue {

    // creates new queue with given capacity
    constructor(maxCapacity) {
        this.priorityValues = new Array(maxCapacity + 1); // array of values
        this.dataKeys = new Array(maxCapacity + 1); // array of keys
        this.idxInPriorityValues = new Array(maxCapacity); // array for getting index of some key
        this.lastValuePointer = 1;
    }

    // pushes key with priority into the queue
    push(key, priority) {
        this.priorityValues[this.lastValuePointer] = priority; // adds to values
        this.dataKeys[this.lastValuePointer] = key; // adds to keys
        this.idxInPriorityValues[key] = this.lastValuePointer; // connect key with pointer
        let cur = this.lastValuePointer;
        let parent = cur >> 1; // whole number division
        // find its place in queue
        while (cur > 1 && this.priorityValues[cur] <= this.priorityValues[parent]) {
            let tmp;
            this.idxInPriorityValues[this.dataKeys[parent]] = cur;
            this.idxInPriorityValues[this.dataKeys[cur]] = parent;
            tmp = this.priorityValues[parent];
            this.priorityValues[parent] = this.priorityValues[cur];
            this.priorityValues[cur] = tmp;
            tmp = this.dataKeys[parent];
            this.dataKeys[parent] = this.dataKeys[cur];
            this.dataKeys[cur] = tmp;
            cur = parent;
            parent = parent >> 1;
        }
        this.lastValuePointer++; // increment pointer for next element
    }

    // get next value with highest priority (lowest value)
    pop() {
        this.lastValuePointer--;
        let ret = this.dataKeys[1];
        this.priorityValues[1] = this.priorityValues[this.lastValuePointer];
        this.dataKeys[1] = this.dataKeys[this.lastValuePointer];
        this.down(); // fix priority queue after popping
        return ret;
    }

    // update key with new priority
    update(key, newPriority) {
        let cur = this.idxInPriorityValues[key];
        let parent = cur >> 1;
        this.priorityValues[cur] = newPriority; // set new priority
        // fix queue
        while (cur > 1 && this.priorityValues[cur] <= this.priorityValues[parent]) {
            let tmp;
            this.idxInPriorityValues[this.dataKeys[parent]] = cur;
            this.idxInPriorityValues[this.dataKeys[cur]] = parent;
            tmp = this.priorityValues[parent];
            this.priorityValues[parent] = this.priorityValues[cur];
            this.priorityValues[cur] = tmp;
            tmp = this.dataKeys[parent];
            this.dataKeys[parent] = this.dataKeys[cur];
            this.dataKeys[cur] = tmp;
            cur = parent;
            parent = parent >> 1;
        }
    }

    // utility function for fixing queue when we pop an element
    down() {
        let cur = 1, left = 2; // init values
        while (left < this.lastValuePointer) { // while queue isn't OK
            let right = left + 1;
            let best = cur;
            if (this.priorityValues[cur] > this.priorityValues[left]) {
                best = left;
            }
            if (right < this.lastValuePointer) {
                if (this.priorityValues[best] > this.priorityValues[right]) {
                    best = right;
                }
            }
            if (best !== cur) { // if queue isn't OK swap and continue in while cycle
                let tmp;
                this.idxInPriorityValues[this.dataKeys[best]] = cur;
                this.idxInPriorityValues[this.dataKeys[cur]] = best;
                tmp = this.priorityValues[best];
                this.priorityValues[best] = this.priorityValues[cur];
                this.priorityValues[cur] = tmp;
                tmp = this.dataKeys[best];
                this.dataKeys[best] = this.dataKeys[cur];
                this.dataKeys[cur] = tmp;
                cur = best;
                left = 2 * cur;
            } else {
                break;
            }
        }
    }
}

/**
 * Class with implementation of open list
 */
class OpenList {
    // m -> rows, n -> columns
    constructor(m, n) {
        this.n = n;
        this.priorityQueue = new IndexedPriorityQueue(m * n); // create new priority queue with m * n elements
        this.nodeArray = new Array(m); // array containing nodes
        for (let i = 0; i < m; i++) {
            this.nodeArray[i] = new Array(n);
            for (let j = 0; j < n; j++) {
                this.nodeArray[i][j] = {x: j, y: i, cost: 0, heur: 0, visited: false, from: null}; // node content
            }
        }
        this.size = 0;
    }

    // return true if node with coordinates x and y is in open list, false otherwise
    contains(x, y) {
        return this.nodeArray[y][x].visited;
    }

    // adds node to open list
    add(x, y, cost, heur, from_x, from_y) {
        this.size++;
        this.priorityQueue.push(x + y * this.n, heur); // push (key, value)
        this.nodeArray[y][x].cost = cost;
        this.nodeArray[y][x].heur = heur;
        this.nodeArray[y][x].visited = true;
        this.nodeArray[y][x].from = this.nodeArray[from_y][from_x];
    }

    // updates node in open list
    update(x, y, cost, heur, from_x, from_y) {
        this.priorityQueue.update(x + y * this.n, heur);
        this.nodeArray[y][x].cost = cost;
        this.nodeArray[y][x].heur = heur;
        this.nodeArray[y][x].from = this.nodeArray[from_y][from_x];
    }

    isEmpty() {
        return this.size === 0;
    }

    // returns next node with highest priority (lowest value)
    getNext() {
        this.size--;
        const idx = this.priorityQueue.pop();
        const x = idx % this.n;
        const y = Math.floor(idx / this.n);
        return this.nodeArray[y][x];
    }

    getNodeAt(x, y) {
        return this.nodeArray[y][x];
    }

    // returns heuristic cost of this node
    getHeuristicAt(x, y) {
        return this.nodeArray[y][x].heur;
    }
}

/**
 * Class with implementation of closed list
 */
class ClosedList {
    // m -> rows, n -> columns
    constructor(m, n) {
        this.array = new Array(m); // boolean 2D array
        for (let i = 0; i < m; i++) {
            this.array[i] = new Array(n);
            for (let j = 0; j < n; j++) {
                this.array[i][j] = false;
            }
        }
    }

    // adds to closed list
    add(x, y) {
        this.array[y][x] = true;
    }

    // returns true if closed list contains element with coords x and y, false otherwise
    contains(x, y) {
        return this.array[y][x];
    }
}