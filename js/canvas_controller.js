// global constants for canvas configuration
const CANVAS_RATIO = 0.75; // how much space does the canvas take in page
const BORDER_WIDTH = 3;
const BORDER_RADIUS = 8;
const LINE_WIDTH = 1;
const SQUARE_OFFSET = 2; // how many pixels are in between of 2 squares in maze
const BG_COLOR = '#fffaeb';
// colors
const SQUARE_COLOR_WALL = '#000000';
const SQUARE_COLOR_START = '#ffe400';
const SQUARE_COLOR_DESTINATION = '#ff0019';
const SQUARE_COLOR_CLOSED_LIST = '#00ff0c';
const SQUARE_COLOR_OPEN_LIST = '#00ffe4';
const SQUARE_COLOR_PATH = '#0200ff';
const LINE_COLOR = '#000000';

/**
 * Class for creating and manipulating with canvas
 */
class CanvasController {
    constructor(n, m) {
        // number of rows and columns
        this.n = n;
        this.m = m;
    }

    // initializes canvas
    init() {
        // create canvas and set id
        this.canvas = document.createElement('canvas');
        this.canvas.setAttribute('id', 'maze_canvas');
        // get available width and height of the page
        const width = window.innerWidth;
        const height = window.innerHeight - document.getElementById('slider_wrap').clientHeight;
        // compute padding
        const padding = (1 - CANVAS_RATIO) / 2;
        // find out which orientation is better for current setting (rows and columns)
        if (width * this.m >= this.n * height) {
            // portrait orientation
            // compute canvasHeight and width and set it to canvas
            const canvasHeight = Math.round(CANVAS_RATIO * height);
            const canvasWidth = Math.round(this.n * (canvasHeight / this.m));
            this.canvas.setAttribute('height', canvasHeight.toString());
            this.canvas.setAttribute('width', canvasWidth.toString());

            // compute its position
            this.canvas.style.left = 100 * ((width - (this.n * (canvasHeight / this.m))) / 2) / width + '%';
            this.canvas.style.top = 100 * padding + '%';
        } else {
            // landscape orientation
            const canvasWidth = Math.round(CANVAS_RATIO * width);
            const canvasHeight = Math.round(this.m * (canvasWidth / this.n));
            this.canvas.setAttribute('height', canvasHeight.toString());
            this.canvas.setAttribute('width', canvasWidth.toString());

            this.canvas.style.left = 100 * padding + '%';
            this.canvas.style.top = 100 * ((height - (this.m * (canvasWidth / this.n))) / 2) / height + '%';
        }
        // set css properties
        this.canvas.style.setProperty('position', 'fixed');
        this.canvas.style.setProperty('border-radius', BORDER_RADIUS + 'px');
        this.canvas.style.setProperty('border', BORDER_WIDTH + 'px solid #000000');
        // append canvas to body
        document.body.appendChild(this.canvas);
        // save some useful values
        this.squareSizeFloat = this.canvas.width / this.n;
        this.squareSize = Math.round(this.squareSizeFloat);
        this.context = this.canvas.getContext('2d');
    }

    // draw grid on the canvas (rows * columns)
    grid() {
        this.context.beginPath();
        // draw horizontal lines
        for (let i = 0; i < this.m; i++) {
            this.context.moveTo(0, Math.round(i * this.squareSizeFloat));
            this.context.lineTo(this.canvas.width, Math.round(i * this.squareSizeFloat));
        }
        // draw vertical lines
        for (let i = 0; i < this.n; i++) {
            this.context.moveTo(Math.round(i * this.squareSizeFloat), 0);
            this.context.lineTo(Math.round(i * this.squareSizeFloat), this.canvas.height);
        }
        // set properties
        this.context.strokeStyle = LINE_COLOR;
        this.context.lineWidth = LINE_WIDTH;
        this.context.closePath();
        this.context.stroke();
    }

    // fill background with BG_COLOR
    bg() {
        this.context.fillStyle = BG_COLOR;
        this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);
    }

    // draws square at given coordination
    drawSquareAt(x, y, color, squareOffset) {
        this.context.fillStyle = color;
        this.context.fillRect(Math.round(x * this.squareSizeFloat) + squareOffset, Math.round(y * this.squareSizeFloat) + squareOffset,
                              this.squareSize - 2 * squareOffset, this.squareSize - 2 * squareOffset);
    }
}
