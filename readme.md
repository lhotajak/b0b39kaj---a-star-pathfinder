# A* Pathfinder
### Jakub Lhoták
##### ČVUT FEL - B0B39KAJ

Cílem tohoto projektu bylo vytvoření interaktivní webové aplikace v JavaScriptu pro předmět B0B39KAJ 
(Tvorba klientských aplikací v JavaScriptu). Mohli jsme si zvolit libovolné téma práce, a protože
jsem si ve čtvrtém semestru zapsal předmět B4B36ZUI (Základy umělé inteligence), napadlo mě si jako
téma práce zvolit vizualizaci jednoho z probíraných algoritmů, algoritmus sloužící k hledání nejkratší
cesty v ohodnoceném grafu, A* (A star). <br><br>

Při práci na tomto projektu jsem se naučil mnoho užitečných způsobů jak programovat nejen v JavaScriptu,
ale i v jiných jazycích a knihovnách (HTML 5, CSS 3, jQuery... ). Svůj projekt jsem programoval s
jasným cílem, tedy vytvořit fungující hledač cesty. Když už jsem měl toto hotové, tak jsem přidával
různou funkcionalitu z tabulkového zadání. Snažil jsem se, aby tam bylo od všeho něco, a tak se může
stát, že některé komponenty mé aplikace do ní úplně nezapadají. Neměly by ale překážet a pouze slouží
k demonstraci použitých technologií. Kód je psaný v angličtině za použití standardu ES6. Byl
testován na několika prohlížečích a kromě Internet Exploreru funguje naprosto bez problému. V kódu
jsou komentáře, všechny v angličtině. <br>
Aplikace může sloužit k výukovým účelům. Lze si zde snadno vyzkoušet jak A* funguje. <br><br>

##### Popis funkčnosti:

Kostra celého projektu je jeden html soubor (index.html), po jehož spuštění se v prohlížeči načte
hlavní menu. V tomto menu můžete zadat rozměry pro generované bludiště. Poté co zadáte rozměry (nebo
necháte základní) a kliknete na tlačítko 'Generate' se vám vygeneruje Html5 Canvas, na kterém bude
nakreslena mřížka, oddělující jednotlivá políčka bludiště. Všechny políčka budou prázdná, kromě dvou:
startovní (žluté) a cílové (červené). V aplikaci můžete libovolně manipulovat s pozicí startovních
a cílových políček pomocí tlačítek 'Set Start' a 'Set Destination'. Do bludiště můžete libovolně 
kreslit překážky (stačí podržet tlačítko myši a kreslit po Canvasu). Pokud nakreslíte něco
a chcete to smazat, můžete použít tlačítko 'Delete', kterým změníte chování aplikace. Nyní místo kreslení
překážek budete překážky mazat. Kliknutím na toto tlačítko po druhé aplikaci zase přepnete do
standardního kreslícího režimu. Ve chvíli, kdy chcete simulaci spustit, klikněte na tlačítko 'Start'.
V dolní části obrazovky můžete nastavit rychlost simulace. Pokud chcete uprostřed simulace na chvíli zastavit a případně něco dokreslit,
můžete k tomu využít tlačítko 'Pause'. Dále je zde tlačítko 'Clear', pomocí něhož smažete všechny
nakreslené překážky. Pokud si chcete nakreslené bludiště uložit do svého lokálního uložiště
(localStorage) použijte tlačítko 'Save to Local Storage'. <br>
Pro návrat zpět do hlavního menu použijte tlačítko domečku vlevo nahoře. Zde si ještě můžete načíst
některá z vašich uložených bludiští. Klikněte na 'Load from Local Storage' a pokud máte nějaké
bludiště uložené, tak se vám zde zobrazí. Pomocí tlačítka 'Load' ho načtete do
Canvasu a pomocí 'Delete' ho smažete. <br>
V aplikaci si můžete vyzkoušet i jiné funkce. Například po stisknutí na ikonu reproduktoru vpravo nahoře
zapnete/vypnete zvuk. Vlevo dole se nachází SVG prvek, na který když kliknete, tak vám dá vědět, zda
jste připojen k síti, nebo ne. Tato utilitka mi pořádně fungovala jenom při použití Microsoft Edge
(to ovšem může být nastavením síťových připojení na mém počítači a nikoli chybou prohlížečů).
Dále se v aplikaci nachází jednoduchý přehrávač hudby s jednou jedinou nahrávkou, tu si člověk může
pustit při testování celé aplikace.
